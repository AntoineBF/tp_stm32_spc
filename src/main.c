#include <stdio.h>
#include <math.h>
#include <string.h>
#include "sys/cm4.h"
#include "sys/devices.h"
#include "sys/init.h"
#include "sys/clock.h"

#define TIMETRAVEL 1000

static volatile char c=0;
volatile int tmps = 5600;
volatile uint32_t chrono = 0;
volatile char tmp;

void init_LD2(){
	RCC.AHB1ENR |= 0x01;
	GPIOA.MODER = (GPIOA.MODER & 0xFFFFF3FF) | 0x00000400;
	GPIOA.OTYPER &=~(0x1<<5);
	GPIOA.OSPEEDR |= 0x03<<10;
	GPIOA.PUPDR &= 0xFFFFF3FF;
}

void init_PB(){
	GPIOC.MODER = (GPIOC.MODER & 0xF3FFFFFF) ;
}

void led_on(){
	GPIOA.ODR |= 0x0020;
}

void led_off(){
	GPIOA.ODR &= 0xFFDF;
}

void tempo_500ms(){
	volatile uint32_t duree;
	for (duree = 0; duree < 5600000 ; duree++){
		;
	}
}

void tempo_ms_led(int stat, int tmps){
	volatile uint32_t duree;
	for (duree = 0; duree < tmps *1000 ; duree++){
		if(stat == 1){
			led_on();
		}else{
			led_off();
		}
	}
}

void tempo_500ms_for_button(){
	volatile uint32_t duree;
	for (duree = 0; duree < 2800000 ; duree++){
		led_on();
	}
	for (duree = 0; duree < 2800000 ; duree++){
		led_off();
	}
}

void button(){
	if(!((GPIOC.IDR >> 13) > 0)){
		_puts("Bouton appuyé\n");
		_putc(0x0D);
		while(!((GPIOC.IDR >> 13) > 0)){
				led_on();
		}
		for(int i = 0; i < 4; i++){
			tempo_500ms_for_button();
		}
	}
}

int set_button(){
	if(!((GPIOC.IDR >> 13) > 0)){
		_puts("Bouton appuyé\n");
		return 1;
	}else{
		return 0;
	}
}

void init_USART(){
	GPIOA.MODER = (GPIOA.MODER & 0xFFFFFF0F) | 0x000000A0;
	GPIOA.AFRL = (GPIOA.AFRL & 0xFFFF00FF) | 0x00007700;
	USART2.BRR = get_APB1CLK()/9600;
	USART2.CR3 = 0;
	USART2.CR2 = 0;
}

void _putc(char c){
	while( (USART2.SR & 0x80) == 0);  
	USART2.DR = c;
}

void _puts(char *c){
	int len = strlen(c);
	for (int i=0;i<len;i++){
		_putc(c[i]);
	}
}

char _getc(){
	while ( (USART2.SR & 0x20) == 0); 
	return USART2.DR;
}

void afficher_chrono(int msec, int sec, int min){
	printf("%d:%d:%d      ",min,sec,msec);
	_putc(0x0D);
}

void systick_init(uint32_t freq){
	uint32_t p = get_SYSCLK()/freq;
	SysTick.LOAD = (p-1) & 0x00FFFFFF;
	SysTick.VAL = 0;
	SysTick.CTRL |= 7;
}

void set_systick_val(uint32_t counter){
	SysTick.VAL = counter;
}

void __attribute__((interrupt)) SysTick_Handler(){
	//printf("test __attribute__\n");
	/*
	tempo_ms_led(1, tmps);
	tempo_ms_led(0, tmps);
	if(!((GPIOC.IDR >> 13) > 0)){
		tmps -= TIMETRAVEL;
	}*/
	
	chrono++;
	
}

int main() {
  
	printf("\e[2J\e[1;1H\r\n");
	printf("\e[01;32m*** Welcome to Nucleo F446 ! ***\e[00m\r\n");
	
		printf("\e[01;31m\t%08lx-%08lx-%08lx\e[00m\r\n",
			   U_ID[0],U_ID[1],U_ID[2]);
		printf("SYSCLK = %9lu Hz\r\n",get_SYSCLK());
		printf("AHBCLK = %9lu Hz\r\n",get_AHBCLK());
		printf("APB1CLK= %9lu Hz\r\n",get_APB1CLK());
		printf("APB2CLK= %9lu Hz\r\n",get_APB2CLK());
		printf("\r\n");
		
	init_PB();
	init_LD2();
	init_USART();
	
	printf("		/****/ Welcome to CHRONO /****/\n");
	_putc(0x0A);
	_putc(0x0D);
	printf("Press l to launch\n");
	_putc(0x0A);
	_putc(0x0D);
	printf("Press button to stop\n");
	_putc(0x0A);
	_putc(0x0D);
	printf(	"Press r to reset\n");
	_putc(0x0A);
	_putc(0x0D);
	
	/*		PROJET - CHRONO		*/
	tmp = NULL;
	int sec = 0;
	int min = 0;
	while(1){
		if(tmp != 'l'){
			tmp = _getc();
		}
		if(tmp == 'l'){
			while(set_button() == 0){		//tmp != 'a' 
				printf("%c: ",tmp);
				systick_init(1000);
				int countflag = (SysTick.CTRL >> 16) & 1;
				if(chrono >= 1000){
					chrono = chrono %1000;
					sec++;
				}
				if(sec == 60){
					sec = 0;
					min++;
				}
				afficher_chrono(chrono, sec, min);
			}
			tmp = 'a';
			while(tmp != 'l' | tmp != 'r'){
				printf("%c: \n",tmp);
				_putc(0x0D);
				tmp = _getc();
				if(tmp == 'r'){
					chrono = 0;
					sec = 0;
					min = 0;
				}
			}
		}
		
	}
	
	
	/*		AFFICHAGE - GTK TERM		*/
	/*
	_putc('E');
	tempo_500ms();
	_puts("... Welcome ");
	_puts(61);
	_putc(0x0A);
	_putc(0x0D);
	_puts("61");
	tempo_500ms();
	_putc(0x0A);
	_putc(0x0A);
	_putc(0x0D);
	tempo_500ms();
	_putc('O');
	tempo_500ms();
	_putc('N');
	_putc(0x0A);
	_putc(0x0D);
	char tmp = 'e';
	_puts("Vous pouvez écrire dans la page GtkTerm.");
	_putc(0x0A);
	_putc(0x0D);
	_puts("Pour utiliser le bouton, appuyer 2");
	_putc(0x0A);
	_putc(0x0D);
	while (tmp != '2'){
		if(tmp == 13){
			_putc(0x0A);
		}
		tmp = _getc();
		_putc(tmp);
	}
	*/
	/*		BOUTON - LED		*/
	/*while (1){
		_putc(0x0A);
		_putc(0x0D);
		button();
	}*/

	/*		UTILISATION INTERRUPT SYSTICK		*/
	/*
	systick_init(50);
	int countflag = (SysTick.CTRL >> 16) & 1;
	set_systick_val(10);
	*/
	/*
	while(countflag == 1){ 
		tempo_ms_led(1, tmps);
		tempo_ms_led(0, tmps);
	}*/
	/*			RALENTIR FREQ SANS INTERRUPT 		*/
	/*
	char tmp = NULL;
	while(1){
		tempo_ms_led(1, tmps);
		tempo_ms_led(0, tmps);
		tmp = _getc();
		if(tmp != NULL){
			tmps += TIMETRAVEL;
		}
	}
	*/
		
	while (1);
	return 0;
}

