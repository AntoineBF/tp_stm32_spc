SPC  Compte rendu de TP STM32

# Compte-rendu de TP STM32

Sujet choisi : Chronomètre

Lien du git : [https://gitlab.com/AntoineBF/tp_stm32_spc](https://gitlab.com/AntoineBF/tp_stm32_spc)

### Participants 

* Antoine Bonfils
* Julien Ferrari

## 1 Prise en main de la carte
Mémo : pour lancer la compilation, on utilise make
    , pour transférer, on utilise make load

### 1.1 Les GPIO

On commence par récupérer les pattes sur lesquelles sont situés le bouton poussoir et la `LED 2` :

Bouton poussoir sur GPIOC patte `13`.

LED2 sur GPIOA patte `5`.

On va donc lire le 14ème bit de GPIOC et écrire dans le 6ème bit de GPIOA.

GPIOC.IDR = `0x00 00 20 00 `

GPIOA.ODR = `0x00 00 00 20`

On veut regarder la valeur du `14 ème bit` sans toucher aux autres. 

Pour cela on fait un décalage de `13 bits` et un test de valeur sur GPIOC.

De même, on veut pouvoir écrire le `6ème bit` de GPIOA sans toucher aux autres. 

On applique un masque OU de valeur `0x00000020` sur place afin de mettre uniquement ce bit à `1`.

Pour faire clignoter la LED, on écrit `1` dans le bon bit pour une certaine durée, puis on met `0` etc.


Pour la dernière question de cette partie, nous décidons de diviser notre code en plusieurs fonctions pour plus de lisibilité et de praticité.

Une fois les fonctions implémentées, on fait un premier test pour savoir si le `bouton` a été appuyé puis un deuxième pour savoir s' il est maintenu.

Quand on appuie sur le bouton alors que la LED est éteinte, elle s’allume. Puis quand on le relâche, elle clignote pour une certaine durée.

### 1.2 Mise en oeuvre de l’USART

Pour se connecter on lance `gtkterm & ` dans le terminal.

Puis, on configure le port dans le menu `>> configuration >> port` pour se mettre sur `/dev/ttyACM0`.

Pour afficher un caractère, on utilise la fonction `putc()` :
```
_putc(“E”);

_putc(“E”);     affichera     EE
```
Les valeurs `0x0A` et `0x0D` permettent de déplacer le curseur :
```
_putc(“E”);                  

_putc(“0x0D”);    affichera    E

_putc(“E”);            E
```

Pour saisir un caractère au clavier on utilise `getc()`:

`gect()` récupérera le premier caractère tapé au clavier.

Pour l’afficher on fait :
```
_putc(_get()); si on tape “E” au clavier, cela affichera E.
```
L’affichage d’un entier sur 2 digits avec la fonction `puts()` donne un résultat hasardeux. 

La fonction `puts()` prenant un char*, si on écrit `puts(22)` la machine va afficher la valeur contenue dans la case à l’adresse `22` jusqu’à tomber sur un code de fin de chaîne.

## 2 Exercice de synthèse
### 2.1 Projet par défaut : Chronomètre piloté par le clavier

Notre projet est de coder un chronomètre sur `une carte STM32`. Le chronomètre devra afficher les millisecondes, les secondes, et les minutes. Avec la possibilité, pour l'utilisateur, de le lancer/démarrer, l'arrêter, et le réinitialiser. 

Voir l'automate avec les trois états pour avoir connaissance des possibilités à chaque état.

![Automate du chronomètre](automate_chrono.png)


## Les composants de la carte qui sont utilisés par notre projet

- Pour contrôler le chronomètre, nous utilisons le `bouton` intégré dans la carte pour l'arrêter. 
- Et également, le clavier avec les fonctions `_getc()` et `_putc()` (Data et Status registers de USART). Plus précisément, la touche `l` pour lancer/démarrer et la touche `r` pour reset.


## À quoi servent les interruptions dans notre projet

- Pour notre projet, nous avons utiliser une interruption du `systick` pour incrémenter le chronomètre toutes les millisecondes.


## L'état de notre projet / les résultats obtenus

- Nous avons pu afficher le chronomètre, le lancer, l'arrêter, le reprendre, le reset, et le relancer par la suite. Donc, toutes les fonctionnalités d'un vrai chronomètres sont ici.

## Les difficultés rencontrées.
- L'assimilation des documents propres à la carte a été difficile car ces derniers sont très garnis. Il a souvent été fastidieux de retrouver les notions et points intéressants à la réalisation de notre projet.

- La compréhension et la bonne utilisation des interruptions ainsi que leur impact sur l'éxécution du code n'étaient pas évidentes au début. On les a d'abord utilisé pour afficher la valeur du temps lors de l'arrêt du chronomètre, mais cela affectait le bon déroulé du programme qui créait un retard par rapport au temps réel écoulé. Nous avons décidé de compter les milisecondes à l'aide des interruptions. 